import { Dispatch, createContext, useReducer } from 'react';

export type User = {
  name: string;
  docRef: string;
  gender: string;
  contact: string;
  groupe: string;
  birthDate: string;
  typeOfDesease: string;
};

type Devis = {
  title: string;
  subtitle: string;
  pharmacie: string;
};

type UserState = {
  user: User;
  token: string;
  isAuth: boolean;
  devis?: Devis;
};

export enum UserAction {
  SET_USER,
  SET_USER_AUTH,
  SET_TOKEN,
  SET_DEVIS,
  ON_AUTH,
  CLEAN,
}

type UserStateAction =
  | { type: UserAction.SET_TOKEN; payload: string }
  | { type: UserAction.SET_USER; payload: User }
  | { type: UserAction.SET_USER_AUTH; payload: boolean }
  | { type: UserAction.ON_AUTH; payload: User }
  | { type: UserAction.SET_DEVIS; payload: Devis }
  | { type: UserAction.CLEAN };

type Context = {
  state: UserState;
  dispatch: Dispatch<UserStateAction>;
};

export const UserContext = createContext({} as Context);

const initialValue = {
  token: '',
  user: {} as User,
  isAuth: false,
};

function userContextReducer(
  state: UserState,
  action: UserStateAction
): UserState {
  switch (action.type) {
    case UserAction.SET_TOKEN:
      return { ...state, token: action.payload };
    case UserAction.SET_USER:
      return { ...state, user: action.payload };
    case UserAction.SET_USER_AUTH:
      return { ...state, isAuth: action.payload };
    case UserAction.SET_DEVIS:
      return { ...state, devis: action.payload };
    case UserAction.ON_AUTH:
      return { ...state, user: action.payload, isAuth: true };
    case UserAction.CLEAN:
      return initialValue;
    default:
      throw new Error();
  }
}

export const UserContextProvider = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  const [state, dispatch] = useReducer(userContextReducer, initialValue);

  return (
    <UserContext.Provider value={{ state, dispatch }}>
      {children}
    </UserContext.Provider>
  );
};
