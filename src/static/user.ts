export const USERS = [
  {
    name: 'MOUSSA HADIZA',
    docRef: 'NE1530',
    gender: 'Femme',
    contact: '10101010',
    groupe: 'AB+',
    birthDate: '20 octobre 2011',
    typeOfDesease: 'Homozygote SS',
  },
  {
    name: 'ABDOULKADRI HALIDOU',
    docRef: 'NE11',
    gender: 'Homme',
    contact: '00000000',
    groupe: 'B+',
    birthDate: '26 Mars 2009',
    typeOfDesease: 'Homozygote SS',
  },
];
