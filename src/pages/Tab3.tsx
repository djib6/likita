import {
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonContent,
  IonHeader,
  IonPage,
  IonSearchbar,
  IonText,
  IonTitle,
  IonToolbar,
  SearchbarChangeEventDetail,
  useIonAlert,
} from '@ionic/react';
import { useCallback, useMemo, useState } from 'react';
import { CallNumber } from '@awesome-cordova-plugins/call-number';
import { HOSPITALS } from '../static/hospital';
import './Tab3.css';

const Tab3: React.FC = () => {
  const [searchText, setSearchText] = useState('');
  const [present] = useIonAlert();
  const handleChange = useCallback(
    (e: CustomEvent<SearchbarChangeEventDetail>) => {
      setSearchText(e.detail.value!);
    },
    []
  );

  const renderHospital = useMemo(
    () =>
      HOSPITALS.filter(
        ({ name, specialty }) =>
          name.toLowerCase().includes(searchText.toLowerCase()) ||
          specialty.toLowerCase().includes(searchText.toLowerCase())
      ).map(({ name, address, id, closed, open, contact, specialty }) => (
        <IonCard
          key={id}
          onClick={() => {
            present({
              cssClass: 'my-css',
              header: 'Info',
              message: 'Appeler pour prendre rendez-vous?',
              buttons: [
                'Non',
                {
                  text: 'Oui',
                  handler: (d) => {
                    CallNumber.callNumber(contact, true);
                  },
                },
              ],
            });
          }}
        >
          <IonCardHeader>
            <IonCardSubtitle>
              <IonText color="secondary">
                <p>{specialty}</p>
              </IonText>
            </IonCardSubtitle>
            <IonCardTitle>{name}</IonCardTitle>
          </IonCardHeader>

          <IonCardContent>
            <p>
              <b>Adresse</b>: {address}
            </p>
            <IonText color="primary">
              <p>Tel: {contact}</p>
            </IonText>
            <IonText>
              <p>
                Horaire: Ouvert de {open} à {closed}
              </p>
            </IonText>
          </IonCardContent>
        </IonCard>
      )),
    [present, searchText]
  );

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Hopitaux</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Hopitaux</IonTitle>
          </IonToolbar>
        </IonHeader>
        <div style={{ padding: 10 }}>
          <IonSearchbar
            value={searchText}
            onIonChange={handleChange}
            showCancelButton="focus"
            animated
          ></IonSearchbar>
          <IonText color="primary">
            <h4>Annuaire des Centre de santé</h4>
          </IonText>
          {renderHospital}
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Tab3;
