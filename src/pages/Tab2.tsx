import {
  IonCheckbox,
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonItem,
  IonText,
  IonLabel,
  IonPage,
  IonRow,
  IonSearchbar,
  IonTitle,
  IonToolbar,
  SearchbarChangeEventDetail,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonList,
} from '@ionic/react';
import { useCallback, useMemo, useState } from 'react';
import { PHARMACIES } from '../static/pharmacie';
import './Tab2.css';

const Tab2: React.FC = () => {
  const [searchText, setSearchText] = useState('');
  const [deGarde, setDeGarde] = useState(false);

  const handleChange = useCallback(
    (e: CustomEvent<SearchbarChangeEventDetail>) => {
      setSearchText(e.detail.value!);
    },
    []
  );

  const renderPharm = useMemo(
    () =>
      PHARMACIES.filter(({ name, status }) => {
        if (deGarde) {
          return (
            name.toLowerCase().includes(searchText.toLowerCase()) &&
            status === true
          );
        }
        return name.toLowerCase().includes(searchText.toLowerCase());
      }).map(({ name, status, id, adresse, assurance, contact }) => (
        <IonCard
          key={id}
          // onClick={() => {
          //   present({
          //     cssClass: 'my-css',
          //     header: 'Alerte',
          //     message: 'Envoyer une demande de devis?',
          //     buttons: [
          //       'Non',
          //       {
          //         text: 'Oui',
          //         handler: (d) => {
          //           presentLoading({
          //             message: 'Chargement...',
          //           });
          //           setTimeout(() => {
          //             dismmissLoading();
          //             const date = new Date();
          //             dispatch({
          //               type: UserAction.SET_DEVIS,
          //               payload: {
          //                 pharmacie: name,
          //                 subtitle: `${date.getDate()}/${
          //                   date.getMonth() + 1
          //                 }/${date.getFullYear()}`,
          //                 title: name,
          //               },
          //             });
          //             presentToast({
          //               buttons: [
          //                 { text: 'Cacher', handler: () => dismissToast() },
          //               ],
          //               message: 'Votre demande a été reçue',
          //               duration: 3000,
          //               position: 'top',
          //               color: 'primary',
          //             });
          //           }, 3000);
          //         },
          //       },
          //     ],
          //   });
          // }}
        >
          <IonCardHeader>
            {status && (
              <IonCardSubtitle>
                <IonText color="secondary">
                  <p>Garde</p>
                </IonText>
              </IonCardSubtitle>
            )}
            <IonCardTitle>{name}</IonCardTitle>
          </IonCardHeader>

          <IonCardContent>
            <p>Assurances:</p>
            <IonList>
              {assurance.map((item) => (
                <IonItem key={item + id}>
                  <IonLabel>{item}</IonLabel>
                </IonItem>
              ))}
            </IonList>
            <IonText color="primary">
              <p>Tel: {contact}</p>
            </IonText>
            <IonText>
              <p>Adresse: {adresse}</p>
            </IonText>
          </IonCardContent>
        </IonCard>
      )),
    [deGarde, searchText]
  );
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Liste pharmacies</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Liste pharmacies</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonGrid>
          <IonRow>
            <IonCol size="2" style={{ display: 'flex', alignItems: 'center' }}>
              <IonCheckbox
                checked={deGarde}
                onIonChange={(e) => setDeGarde(e.detail.checked)}
              />
            </IonCol>
            <IonCol>
              <IonSearchbar
                value={searchText}
                onIonChange={handleChange}
                showCancelButton="focus"
                animated
              ></IonSearchbar>
            </IonCol>
          </IonRow>
        </IonGrid>
        <IonText color={deGarde ? 'secondary' : 'primary'}>
          <h4>
            {deGarde ? 'Parmacies de garde uniquement' : 'Toutes les pharmcies'}
          </h4>
        </IonText>
        {renderPharm}
      </IonContent>
    </IonPage>
  );
};

export default Tab2;
