import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonText,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonItem,
  IonLabel,
  IonList,
  IonSlide,
  IonSlides,
} from '@ionic/react';
import { useContext } from 'react';
import { UserContext } from '../context/UserContext';
import './Tab1.css';
import m1 from '../static/m1.jpg';
import m2 from '../static/m2.jpg';
import m3 from '../static/m3.jpg';

const DEVIS = [
  {
    title: 'Devis du 10/08/2021',
    subtitle: 'Pharmacie Lamordé',
    drugs: [' 1x1 Aspirine', '3x1 paralex'],
    price: '3500 FCFA',
  },
  {
    title: 'Devis du 25/10/2020',
    subtitle: 'Pharmacie Ader',
    drugs: [' 2x1 Boliprine', '3x1 paralex'],
    price: '6825 FCFA',
  },
  // {
  //   title: 'Devis du 17/06/2020',
  //   subtitle: 'Pharmacie Ader',
  //   drugs: [' 1x1 Dupablex', '3x1 paralex'],
  //   price: '4350 FCFA',
  // },
];

const slideOpts = {
  initialSlide: 1,
  speed: 400,
};

const Tab1: React.FC = () => {
  const { state } = useContext(UserContext);
  return (
    <IonPage>
      <IonContent fullscreen>
        <IonHeader>
          <IonToolbar>
            <IonTitle>Acceuil</IonTitle>
          </IonToolbar>
        </IonHeader>
        <div style={{ padding: 10 }}>
          <div>
            <IonText>
              <h2>Ravis de vous revoir,</h2>
            </IonText>
            <IonText color="primary">
              <h1>{state.user.name}</h1>
            </IonText>
          </div>
        </div>
        <IonSlides pager={true} options={slideOpts}>
          <IonSlide>
            <img src={m1} />
          </IonSlide>
          <IonSlide>
            <img src={m2} />
          </IonSlide>
          <IonSlide>
            <img src={m3} />
          </IonSlide>
        </IonSlides>
        <IonCard>
          <IonCardHeader>
            <IonCardSubtitle>
              <IonText color="secondary">
                Moringa l’Authentique réduit les effets du diabète
              </IonText>
            </IonCardSubtitle>
          </IonCardHeader>
          <IonCardContent>
            Une étude scientifique de 2009 a prouvé que la consommation en
            infusion de feuilles de Moringa réduisait le taux de glycémie des
            diabétiques en situation d’hyperglycémie. Ainsi, les résultats de
            cette étude montrent que boire une infusion de Moringa n’a au bout
            de deux heures aucun effet significatif sur le niveau de sucre dans
            le sang des personnes à la glycémie normale (entre 60 et 120 mg /
            dl) mais permettait une baisse de la glycémie de 28,15 mg / dl chez
            les patients hyperglycémiques. L’étude concluait alors qu’une
            thérapie à base d’infusion au Moringa serait souhaitable, car bon
            marché et facile à appliquer, pour les personnes atteintes de
            diabète et soignées par injection d’insuline.
          </IonCardContent>
        </IonCard>
      </IonContent>
    </IonPage>
  );
};

export default Tab1;
