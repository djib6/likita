import {
  InputChangeEventDetail,
  IonButton,
  IonContent,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonText,
  useIonLoading,
} from '@ionic/react';
import { useCallback, useContext, useState } from 'react';
import { RouteComponentProps } from 'react-router';
import { UserAction, UserContext } from '../../context/UserContext';
import { USERS } from '../../static/user';

interface AuthProps extends RouteComponentProps {}
const Auth: React.FC<AuthProps> = ({ history }) => {
  const { dispatch } = useContext(UserContext);
  const [user, setUser] = useState(
    {} as { medicalId: string; password: string }
  );
  const [showError, setShowError] = useState(false);
  const [presentLoading, dismmissLoading] = useIonLoading();
  const onAuth = useCallback(
    (e: React.MouseEvent<HTMLIonButtonElement, MouseEvent>) => {
      if (!user.medicalId) {
        setShowError(true);
      } else {
        setShowError(false);
        presentLoading({
          message: 'Chargement...',
        });
        setTimeout(() => {
          dismmissLoading();
          const data = USERS.find(
            ({ docRef }) => docRef.toLowerCase() === 'ne11'
          );
          if (!data) {
          } else {
            data.name = user.medicalId;
            dispatch({
              type: UserAction.ON_AUTH,
              payload: data,
            });
            history.push('/tab1');
          }
        }, 2000);
      }
    },
    [dismmissLoading, dispatch, history, presentLoading, user.medicalId]
  );

  const handleNumInputChange = useCallback(
    (e: CustomEvent<InputChangeEventDetail>) => {
      setUser({ ...user, medicalId: e.detail.value! });
    },
    [user]
  );
  return (
    <IonPage>
      <IonContent fullscreen>
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            marginTop: '40%',
          }}
        >
          <IonText color="primary">
            <h1 style={{ fontSize: 30, marginBottom: 50 }}>
              Comment puis-je vous appeler?
            </h1>
          </IonText>
        </div>
        <div style={{ padding: 10 }}>
          <IonList>
            <IonItem>
              <IonLabel position="floating">Votre prénom *</IonLabel>
              <IonInput
                value={user.medicalId}
                placeholder="Votre dossier"
                onIonChange={handleNumInputChange}
              ></IonInput>
              {showError && (
                <p style={{ fontSize: 12, color: 'red' }}>Champ obligatoire</p>
              )}
            </IonItem>
          </IonList>
        </div>

        <div style={{ padding: 10 }}>
          <IonButton expand="block" onClick={onAuth}>
            Entrer
          </IonButton>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Auth;
